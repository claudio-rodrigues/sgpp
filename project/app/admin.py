# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from . import models

class ProjetoAdmin (admin.ModelAdmin):
    list_display = ('nome', 'status', 'classificacao', 'orcamento')
    list_filter = ('nome', )

    def save_model(self, request, obj, form, change):
        obj.usuario_responsavel = request.user
        obj.save()

class IndicadorProjetoAdmin (admin.ModelAdmin):
    list_display = ('id_indicador', 'id_projeto', 'fase_projeto')
    list_filter = ('id_indicador', )

class MembroProjetoAdmin (admin.ModelAdmin):
    list_display = ('id_nome', 'id_projeto')
    list_filter = ('id_projeto', )  

class InformarIndicadorAdmin (admin.ModelAdmin):
    list_display = ('id_indicador', 'id_projeto')
    list_filter = ('id_projeto',)

# Register your models here.
admin.site.register(models.Projeto, ProjetoAdmin)
admin.site.register(models.Indicador)
admin.site.register(models.IndicadorProjeto, IndicadorProjetoAdmin)
admin.site.register(models.InformarIndicador, InformarIndicadorAdmin)
admin.site.register(models.MembroProjeto, MembroProjetoAdmin)