# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse
# from .models import Post


# Create your views here.

# def home(request):
#     return HttpResponse('hello word')

# def home_param(request, post_id):
#     return HttpResponse('hello word %s', post_id)

# def post_list(request):
#     # name = 'Guimarães Rosa'
#     # return render(request, 'post_list.html', {'name': name})
#     posts = Post.objects.all()
#     return render(request, 'post_list.html', {'posts': posts})

# def post_details(request, post_id):
#     post = Post.objects.get(id=post_id)
#     return render(request, 'post_details.html', {'post': post})