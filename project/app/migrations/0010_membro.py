# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2019-11-30 18:02
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('app', '0009_projeto_gerente_responsavel'),
    ]

    operations = [
        migrations.CreateModel(
            name='Membro',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.Projeto')),
                ('projeto', models.ManyToManyField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
