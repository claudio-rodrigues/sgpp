# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

STATUS = (
        (1, 'Em análise'),
        (2, 'Análise realizada'),
        (3, 'Análise aprovada'),
        (4, 'Iniciado planejado'),
        (5, 'Em andamento'),
        (6, 'Encerrado'),
        (7, 'Cancelado'),
    )

CLASSIFICACAO = (
    (1, 'Baixo risco'),
    (2, 'Médio risco'),
    (3, 'Alto risco'),
)

# Create your models here.

class Projeto (models.Model):
    nome = models.CharField(max_length=255)
    gerente_responsavel = models.ForeignKey(User, verbose_name='Gerente responsável')
    data_previsao_inicio = models.DateField(verbose_name='Data inicial')
    data_previsao_final = models.DateField(verbose_name='Data final', null=True, blank=True)
    data_previsao_termino = models.DateField(verbose_name='Data previsão de término')
    data_alteracao = models.DateField(auto_now_add=True)
    orcamento = models.FloatField(verbose_name='Orçamento')
    descricao = models.TextField(verbose_name='Descrição')
    status = models.IntegerField(choices=STATUS, verbose_name='Status')
    classificacao = models.IntegerField(choices=CLASSIFICACAO, verbose_name='Classificação')
    justificativa = models.TextField(null=True, blank=True)

    def validate (self):
        if (self.status == 3 or self.status == 7):
            raise ValidationError('Campo justificativa é obrigatório para o status selecionado')

    def __unicode__(self):
        return self.nome

class Indicador (models.Model):
    nome = models.CharField(max_length=255)

    class Meta:
        verbose_name_plural = 'Indicadores'

    def __str__ (self):
        return self.nome

class IndicadorProjeto (models.Model):
    id_indicador = models.ForeignKey('Indicador', verbose_name='Indicador')
    id_projeto = models.ForeignKey('Projeto', verbose_name='Projeto')
    fase_projeto = models.IntegerField(choices=STATUS)
    valor_minimo = models.IntegerField(verbose_name='Valor mínimo')
    valor_maximo = models.IntegerField(verbose_name='Valor máximo')

    class Meta:
        verbose_name_plural = 'Associar indicadores'
    
    def __str__ (self):
        return str (self.id)

class InformarIndicador (models.Model):
    id_indicador = models.ForeignKey('Indicador', verbose_name='Indicador')
    id_projeto = models.ForeignKey('Projeto', verbose_name='Projeto')

    class Meta:
        verbose_name_plural = 'Informar indicadores'
    
    def __str__ (self):
        return str (self.id)

class MembroProjeto (models.Model):
    id_nome = models.ForeignKey(User, verbose_name='nome')
    id_projeto = models.ForeignKey('Projeto', verbose_name='projeto')

    class Meta:
        verbose_name_plural = 'Membros por projetos'
    

    def __str__ (self):
        return str (self.id)
